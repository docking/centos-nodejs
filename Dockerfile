# centos-nodejs

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.4
ARG CENTOS_VERSION=7
ARG NODEJS_VERSION=12.16.2

ARG KCOV_VERSION=36


FROM ${DOCKER_REGISTRY_URL}centos-nodejs-mini:${CUSTOM_VERSION}-${CENTOS_VERSION}-${NODEJS_VERSION} AS centos-nodejs

ARG KCOV_VERSION

WORKDIR /tmp

RUN \
	yum install -y \
		binutils-devel \
		clang \
		clang-devel \
		cmake \
		curl \
		elfutils-devel \
		gcc \
		gcc-c++ \
		git \
		jemalloc-devel \
		jq \
		libpcap-devel \
		llvm \
		make \
		node-gyp \
		openssl \
		openssl-devel \
		pcre-devel \
		pkgconfig \
		rpm-build \
		rpm-sign \
		unzip \
		xz-devel \
		zip \
		zlib-devel \
	; \
	yum clean all ; \
	rpmdb --rebuilddb

RUN \
	curl -Lo kcov-${KCOV_VERSION}.tar.gz https://github.com/SimonKagstrom/kcov/archive/v${KCOV_VERSION}.tar.gz && ( \
		tar zxf kcov-${KCOV_VERSION}.tar.gz ; \
		mkdir -p kcov-${KCOV_VERSION}/build ; \
		cd kcov-36/build ; \
		cmake .. ; \
		make -j $(nproc) ; \
		make install ; \
	) ; \
	rm -fR kcov-${KCOV_VERSION} kcov-${KCOV_VERSION}.tar.gz
