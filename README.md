# centos-nodejs

A Docker image based on CentOS including a NodeJS install & tools.

Development tools:
 - gcc
 - gcc-c++
 - make
 - clang
 - cmake
 - git
 - kcov (from https://github.com/SimonKagstrom/kcov)
 - llvm
 - make
 - node-gyp
 - pkgconfig

Development libraries:
 - binutils-devel
 - clang-devel
 - elfutils-devel
 - jemalloc-devel
 - libpcap-devel
 - openssl-devel
 - pcre-devel
 - xz-devel
 - zlib-devel

Misc. tools:
 - curl
 - jq
 - rpm-build
 - rpm-sign
 - unzip
 - zip

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`
* Try the docker image: `make shell`
